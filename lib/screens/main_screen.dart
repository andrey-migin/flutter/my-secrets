import 'package:flutter/material.dart';

import '../widgets/menu_widget.dart';
import '../widgets/categories_widget.dart';
import '../widgets/text_widget.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _createMainLayout(),
        MainMenuWidget(),
      ],
    );
  }

  Widget _createMainLayout() {
    return Expanded(
        child: Row(
      children: [
        Container(
          width: 200,
          child: CategoriesListWidget(),
        ),
        Expanded(
          child: Container(
            width: 150,
            height: double.infinity,
            child: TextWidget(),
          ),
        ),
      ],
    ));
  }
}
