import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/auth_provider.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final _form = GlobalKey<FormState>();

  String _passKey;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: _createLayout(context),
    );
  }

  Widget _createLayout(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          height: 150,
          width: 400,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(children: <Widget>[
                Form(
                  key: _form,
                  child: TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Enter your key pass',
                        contentPadding: EdgeInsets.only(top: 0)),
                    textInputAction: TextInputAction.done,
                    validator: (value) {
                      if (value.isEmpty)
                        return "Please type your key pass here";

                      if (value.length < 6)
                        return "Key pass must contain more than 6 symbols";

                      return null;
                    },
                    obscureText: true,
                    onSaved: (value) {
                      _passKey = value;
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 130,
                      vertical: 15,
                    ),
                    child: TextButton(
                      child: Text('Enter the app'),
                      onPressed: () => _submitForm(context),
                    ))
              ])),
        ));
  }

  void _submitForm(BuildContext context) async {
    var isValid = _form.currentState.validate();
    if (!isValid) return;
    _form.currentState.save();

    var result = await context.read<AuthProvider>().login(_passKey);

    if (result != null) {
      await _showErrorDialog(context, result);
      return;
    }
  }

  Future<void> _showErrorDialog(BuildContext context, String message) {
    return showDialog<void>(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text('Error'),
              content: Text(message),
              actions: <Widget>[
                ElevatedButton(
                  child: Text("OK"),
                  onPressed: () {
                    Navigator.of(ctx).pop();
                  },
                )
              ],
            ));
  }
}
