import 'package:flutter/material.dart';
import 'package:my_secrets/providers/auth_provider.dart';
import 'package:provider/provider.dart';
import '../services/saver.dart';
import '../services/settings_reader.dart';

class InitScreen extends StatefulWidget {
  @override
  _InitScreenState createState() => _InitScreenState();
}

class _InitScreenState extends State<InitScreen> {
  final _form = GlobalKey<FormState>();

  String _initVector;

  String _passKey;

  @override
  Widget build(BuildContext context) {
    return _createLayout();
  }

  Widget _createLayout() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          height: 400,
          width: 400,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(children: <Widget>[
                Form(key: _form, child: _createInputs()),
                Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 130,
                      vertical: 15,
                    ),
                    child: TextButton(
                      child: Text('Init'),
                      onPressed: () => _submitForm(),
                    ))
              ])),
        ));
  }

  Widget _createInputs() {
    return Column(
      children: [
        TextFormField(
          decoration: InputDecoration(
              labelText: 'Enter init vector',
              contentPadding: EdgeInsets.only(top: 0)),
          textInputAction: TextInputAction.next,
          validator: (value) {
            if (value.isEmpty) return "Enter init vector here";

            if (value.length == 16 || value.length == 32) {
              return null;
            }
            return "Init vector must have 16 or 32 symbols";
          },
          obscureText: true,
          onSaved: (value) {
            _initVector = value;
          },
        ),
        TextFormField(
          decoration: InputDecoration(
              labelText: 'Type password',
              contentPadding: EdgeInsets.only(top: 0)),
          textInputAction: TextInputAction.done,
          validator: (value) {
            if (value.isEmpty) return "Type password here";
            _passKey = value;
            return null;
          },
          obscureText: true,
          onSaved: (value) {
            _passKey = value;
          },
        ),
        TextFormField(
          decoration: InputDecoration(
              labelText: 'Confirm password',
              contentPadding: EdgeInsets.only(top: 0)),
          textInputAction: TextInputAction.done,
          validator: (value) {
            if (value.isEmpty) return "Confirm password here";

            if (value != _passKey) return "Invalid confirmation password";

            return null;
          },
          obscureText: true,
        ),
      ],
    );
  }

  Future<void> _submitForm() async {
    var validated = _form.currentState.validate();
    if (!validated) return;
    _form.currentState.save();
    var appSettings = AppSettings(initKey: _initVector);

    await SettingsReader.write(appSettings);

    DiskSaver.appSettings = appSettings;

    DiskSaver.saveToDisk(_passKey);

    context.read<AuthProvider>().login(_passKey);
  }
}
