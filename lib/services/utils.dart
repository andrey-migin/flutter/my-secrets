class Utils {
  static String addLastSymbolIfNotExists(String data, String lastSymbol) {
    if (data == null) return lastSymbol;

    if (data.isEmpty) return lastSymbol;

    if (data[data.length - 1] == lastSymbol[0]) return data;

    return data + lastSymbol;
  }
}
