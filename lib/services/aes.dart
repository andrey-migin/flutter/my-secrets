import 'package:encrypt/encrypt.dart' as EncryptPack;
import 'package:crypto/crypto.dart' as CryptoPack;
import 'dart:convert' as ConvertPack;

class Aes256 {
  static String decrypt(String payload, String strPwd, String strIv) {
    var iv = CryptoPack.sha256
        .convert(ConvertPack.utf8.encode(strIv))
        .toString()
        .substring(0, 16); // Consider the first 16 bytes of all 64 bytes
    var key = CryptoPack.sha256
        .convert(ConvertPack.utf8.encode(strPwd))
        .toString()
        .substring(0, 32); // Consider the first 32 bytes of all 64 bytes
    EncryptPack.IV ivObj = EncryptPack.IV.fromUtf8(iv);
    EncryptPack.Key keyObj = EncryptPack.Key.fromUtf8(key);
    final encrypter = EncryptPack.Encrypter(EncryptPack.AES(keyObj,
        mode: EncryptPack.AESMode.cbc)); // Apply CBC mode

    return encrypter.decrypt64(payload, iv: ivObj);
  }

  static String encrypt(String payload, String strPwd, String strIv) {
    var iv = CryptoPack.sha256
        .convert(ConvertPack.utf8.encode(strIv))
        .toString()
        .substring(0, 16); // Consider the first 16 bytes of all 64 bytes
    var key = CryptoPack.sha256
        .convert(ConvertPack.utf8.encode(strPwd))
        .toString()
        .substring(0, 32); // Consider the first 32 bytes of all 64 bytes
    EncryptPack.IV ivObj = EncryptPack.IV.fromUtf8(iv);
    EncryptPack.Key keyObj = EncryptPack.Key.fromUtf8(key);
    final encrypter = EncryptPack.Encrypter(EncryptPack.AES(keyObj,
        mode: EncryptPack.AESMode.cbc)); // Apply CBC mode

    var encryptedResult = encrypter.encrypt(payload, iv: ivObj);

    return encryptedResult.base64;
  }
}
