import 'dart:io';
import 'dart:convert';
import '../services/utils.dart';

import './settings_reader.dart';

import '../services/aes.dart';

enum LoadResult { Ok, InvalidPassword, NewInstance }

class DiskSaver {
  static Map<String, Map<String, String>> data = {};

  static Map<String, Map<String, String>> unsavedData = {};

  static AppSettings appSettings;

  static File openFile() {
    return File(
        Utils.addLastSymbolIfNotExists(Platform.environment['HOME'], "/") +
            ".my-secrets.dat");
  }

  static Future<void> saveToDisk(String passKey) async {
    var jsonOut = {};
    jsonOut.putIfAbsent('data', () => data);

    var jsonToSave = json.encode(jsonOut);

    var file = openFile();
    var dataToSave =
        Aes256.encrypt(jsonToSave, passKey, appSettings.initVector);
    await file.writeAsString(dataToSave);

    print("Saved:" + Platform.environment['HOME']);
    print("Saved:" + dataToSave);
  }

  static Map<String, Map<String, String>> deserialize(data) {
    Map<String, Map<String, String>> result = {};

    data.forEach((key, value) {
      return result.putIfAbsent(key, () {
        Map<String, String> subResult = {};

        value.forEach((subKey, subValue) {
          subResult.putIfAbsent(subKey, () => subValue);
        });

        return subResult;
      });
    });

    return result;
  }

  static Future<LoadResult> loadFromDisk(String passKey) async {
    try {
      var file = openFile();

      if (!await file.exists()) return LoadResult.NewInstance;
      var encrypted = await file.readAsString();
      var loadedJson =
          Aes256.decrypt(encrypted, passKey, appSettings.initVector);
      var jsonAsMap = json.decode(loadedJson);

      data = deserialize(jsonAsMap['data']);
    } catch (e) {
      print(e);
      return LoadResult.InvalidPassword;
    }

    return LoadResult.Ok;
  }
}
