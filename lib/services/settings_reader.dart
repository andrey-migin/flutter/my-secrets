import 'dart:io';
import 'dart:convert';

import 'package:flutter/material.dart';

class AppSettings {
  static final String _initVectorJsonField = "initVector";

  String _initVector;
  String get initVector => _initVector;

  AppSettings.deserializeJson(String jsonString) {
    var jsonAsMap = json.decode(jsonString);
    _initVector = jsonAsMap[_initVectorJsonField];
  }

  String serializeToJson() {
    Map<String, String> jsonAsMap = {};
    jsonAsMap[_initVectorJsonField] = initVector;
    return json.encode(jsonAsMap);
  }

  bool validate() {
    if (initVector == null) return false;

    return true;
  }

  AppSettings({@required String initKey}) {
    _initVector = initKey;
  }
}

class SettingsReader {
  static File _getSettingsFile() {
    return File(Platform.environment['HOME'] + "/setup.json");
  }

  static AppSettings read() {
    try {
      var settingsFile = _getSettingsFile();
      var jsonString = settingsFile.readAsStringSync();

      var result = AppSettings.deserializeJson(jsonString);

      if (!result.validate()) return null;

      return result;
    } catch (e) {
      return null;
    }
  }

  static Future<void> write(AppSettings settings) async {
    var file = _getSettingsFile();
    var json = settings.serializeToJson();
    await file.writeAsString(json);
  }
}
