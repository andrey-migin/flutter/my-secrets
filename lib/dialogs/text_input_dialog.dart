import 'package:flutter/material.dart';

class TextInputDialog extends StatelessWidget {
  final _form = GlobalKey<FormState>();
  String _categoryName;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Form(
        key: _form,
        child: TextFormField(
          decoration: InputDecoration(
              labelText: 'Enter category name',
              contentPadding: EdgeInsets.only(top: 0)),
          textInputAction: TextInputAction.done,
          autofocus: true,
          validator: (value) {
            if (value.isEmpty) return "Please type something here";

            return null;
          },
          onSaved: (value) {
            _categoryName = value;
          },
        ),
      ),
    );
  }

  bool submit() {
    var isValid = _form.currentState.validate();
    if (!isValid) return false;
    _form.currentState.save();
    return true;
  }

  static Future<String> show(BuildContext context) {
    var widget = TextInputDialog();

    return showDialog<String>(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text('Enter Category name'),
              content: widget,
              actions: <Widget>[
                ElevatedButton(
                  child: Text("Save"),
                  onPressed: () {
                    if (widget.submit())
                      Navigator.of(ctx).pop(widget._categoryName);
                  },
                ),
                ElevatedButton(
                  child: Text("Cancel"),
                  onPressed: () {
                    Navigator.of(ctx).pop();
                  },
                )
              ],
            ));
  }
}
