import 'package:flutter/material.dart';

class SearchDialog extends StatelessWidget {
  String _categoryName = "";
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Form(
        child: TextFormField(
          decoration: InputDecoration(
              labelText: 'Enter search a pharse here',
              contentPadding: EdgeInsets.only(top: 0)),
          textInputAction: TextInputAction.search,
          autofocus: true,
          onChanged: (value) {
            _categoryName = value;
          },
        ),
      ),
    );
  }

  static Future<String> show(BuildContext context) {
    var widget = SearchDialog();

    return showDialog<String>(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text('Search'),
              content: widget,
              actions: <Widget>[
                Container(
                  width: 100,
                  child: ElevatedButton(
                    child: Row(
                      children: [
                        Icon(Icons.search),
                        Text("Search"),
                      ],
                    ),
                    onPressed: () {
                      Navigator.of(ctx).pop(widget._categoryName);
                    },
                  ),
                ),
                ElevatedButton(
                  child: Text("Cancel"),
                  onPressed: () {
                    Navigator.of(ctx).pop();
                  },
                )
              ],
            ));
  }
}
