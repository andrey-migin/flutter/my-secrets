import 'package:flutter/material.dart';

class ConfirmSaveDialog extends StatefulWidget {
  @override
  _ConfirmSaveDialogState createState() => _ConfirmSaveDialogState();

  static Future<bool> show(BuildContext context) {
    var widget = ConfirmSaveDialog();

    return showDialog<bool>(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text('Confirmation'),
              content: widget,
              actions: <Widget>[
                ElevatedButton(
                  child: Text("Save"),
                  onPressed: () {
                    Navigator.of(ctx).pop(true);
                  },
                ),
                ElevatedButton(
                  child: Text("Skip"),
                  onPressed: () {
                    Navigator.of(ctx).pop(false);
                  },
                ),
                ElevatedButton(
                  child: Text("Cancel"),
                  onPressed: () {
                    Navigator.of(ctx).pop(null);
                  },
                )
              ],
            ));
  }
}

class _ConfirmSaveDialogState extends State<ConfirmSaveDialog> {
  final _form = GlobalKey<FormState>();

  String _categoryName;

  @override
  Widget build(BuildContext context) {
    return Text("You have unsaved data. Save it?");
  }

  bool submit() {
    var isValid = _form.currentState.validate();
    if (!isValid) return false;
    _form.currentState.save();
    return true;
  }
}
