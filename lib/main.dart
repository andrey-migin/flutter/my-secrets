import 'dart:io';

import 'package:flutter/material.dart';
import 'package:my_secrets/screens/init_screen.dart';
import 'package:my_secrets/services/saver.dart';
import 'package:my_secrets/services/settings_reader.dart';
import 'package:provider/provider.dart';
import '../providers/auth_provider.dart';

import '../providers/categories_provider.dart';
import './screens/main_screen.dart';
import './screens/auth_screen.dart';

import '../states/menu_state.dart';

void main() {
  Platform.environment.forEach((key, value) {
    print("$key $value");
  });

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: CategoriesProvider()),
        ChangeNotifierProvider.value(value: AuthProvider()),
        ChangeNotifierProvider.value(value: MenuState()),
      ],
      child: Consumer<AuthProvider>(builder: (ctx, provider, _) {
        if (provider.authenticated == null) {
          DiskSaver.appSettings = SettingsReader.read();

          if (DiskSaver.appSettings == null)
            return _createInitLayout();
          else
            provider.init();
        }

        return provider.authenticated
            ? _createMainLayout()
            : _createAuthLayout();
      }),
    );
  }

  Widget _createMainLayout() {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }

  Widget _createAuthLayout() {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AuthScreen(),
    );
  }

  Widget _createInitLayout() {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: InitScreen(),
    );
  }
}
