import 'package:flutter/material.dart';
import 'package:my_secrets/services/saver.dart';

class MenuState with ChangeNotifier {
  bool _canCreateSubCategory = false;

  bool get canCreateSubCategory => _canCreateSubCategory;

  void setCanCreateSubCategory(bool state) {
    if (_canCreateSubCategory == state) return;
    _canCreateSubCategory = state;
    notifyListeners();
  }

  bool _hasDataToSave = false;

  bool get hasDataToSave => _hasDataToSave;

  void updateHasDataToSaveState() {
    var state = DiskSaver.unsavedData.length > 0;
    if (hasDataToSave == state) return;
    _hasDataToSave = state;
    notifyListeners();
  }

  String _searchWord = "";

  String get searchWord => _searchWord;

  void setSearchWord(String searchWord) {
    _searchWord = searchWord;
    notifyListeners();
  }
}
