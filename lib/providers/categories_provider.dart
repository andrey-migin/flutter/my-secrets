import 'package:flutter/material.dart';
import '../services/saver.dart';

class CategoriesProvider with ChangeNotifier {
  //// Categories
  void addCategory(String name) {
    DiskSaver.data.putIfAbsent(name, () => {});
    notifyListeners();
  }

  int get categoriesCount => DiskSaver.data.length;

  List<String> getCategories() {
    return DiskSaver.data.keys.toList();
  }

  String _selectedCategory;

  void setSelectedCategory(String selectedItem) {
    _selectedCategory = selectedItem;
    _selectedSubCategory = null;
    _textReadOnly = true;
    notifyListeners();
  }

  String get selectedCategory => _selectedCategory;
  bool get hasSelectedCategory => _selectedCategory != null;

  // SubCategories
  void addSubCategory(String name) {
    if (!hasSelectedCategory) return;
    DiskSaver.data[_selectedCategory].putIfAbsent(name, () => "");
    notifyListeners();
  }

  List<String> getSubCategories() {
    if (_searchWord == null || _searchWord == "")
      return DiskSaver.data[_selectedCategory].keys.toList();

    List<String> result = [];

    DiskSaver.data[_selectedCategory].forEach((key, value) {
      if (value.toLowerCase().indexOf(_searchWord) > -1) result.add(key);
    });

    return result;
  }

  String _selectedSubCategory;

  void setSelectedSubCategory(String selectedItem) {
    _selectedSubCategory = selectedItem;
    _textReadOnly = true;
    notifyListeners();
  }

  String get selectedSubCategory => _selectedSubCategory;
  bool get hasSelectedSubCategory => _selectedSubCategory != null;

  // ActiveText

  String _getText(Map<String, Map<String, String>> map) {
    if (!map.containsKey(_selectedCategory)) return null;
    var subCategory = map[_selectedCategory];

    if (subCategory.containsKey(_selectedSubCategory))
      return subCategory[_selectedSubCategory];

    return null;
  }

  String get activeText {
    if (_selectedCategory == null) return null;
    if (_selectedSubCategory == null) return null;

    var fromUnsaved = _getText(DiskSaver.unsavedData);

    if (fromUnsaved != null) return fromUnsaved;

    return _getText(DiskSaver.data) ?? "";
  }

  void updateText(String value) {
    if (_selectedCategory == null) return;
    if (_selectedSubCategory == null) return;

    DiskSaver.unsavedData.putIfAbsent(_selectedCategory, () => {});

    var subCategory = DiskSaver.unsavedData[_selectedCategory];

    if (subCategory.containsKey(_selectedSubCategory))
      subCategory.update(_selectedSubCategory, (_) => value);
    else
      subCategory.putIfAbsent(_selectedSubCategory, () => value);
  }

  void _flushUnsavedToData() {
    DiskSaver.unsavedData.forEach((category, subCategories) {
      DiskSaver.data.putIfAbsent(category, () => {});

      var subCategoryData = DiskSaver.data[category];

      subCategories.forEach((subCategory, text) {
        if (subCategoryData.containsKey(subCategory))
          subCategoryData.update(subCategory, (_) => text);
        else
          subCategoryData.putIfAbsent(subCategory, () => text);
      });
    });
  }

  void save(String passKey) {
    if (_selectedCategory == null) return null;
    if (_selectedSubCategory == null) return null;

    _flushUnsavedToData();
    DiskSaver.unsavedData.clear();

    DiskSaver.saveToDisk(passKey);

    setTextReadOnly(true);
  }

  bool get hasChanges {
    return DiskSaver.unsavedData.length > 0;
  }

  bool _textReadOnly = true;
  bool get textReadOnly => _textReadOnly;

  void setTextReadOnly(bool value) {
    if (value == _textReadOnly) return;
    _textReadOnly = value;
    notifyListeners();
  }

  String _searchWord;

  void setSearchWord(String searchWord) {
    _searchWord = searchWord.toLowerCase();
    notifyListeners();
  }
}
