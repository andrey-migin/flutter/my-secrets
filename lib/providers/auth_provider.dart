import 'package:flutter/material.dart';
import 'package:my_secrets/services/saver.dart';

class AuthProvider with ChangeNotifier {
  Future<String> login(String passKey) async {
    var authenticated = await DiskSaver.loadFromDisk(passKey);

    if (authenticated == LoadResult.Ok) {
      _authenticated = true;
      _passKey = passKey;

      notifyListeners();
      return null;
    }

    return "Invalid passkey";
  }

  bool _authenticated;

  bool get authenticated {
    return _authenticated;
  }

  void setMode(bool authMode) {
    _authenticated = authMode;
    notifyListeners();
  }

  void logout() {
    setMode(false);
  }

  void init() {
    _authenticated = false;
  }

  String _passKey;

  String get passKey => _passKey;
}
