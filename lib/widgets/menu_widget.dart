import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../dialogs/text_input_dialog.dart';
import '../states/menu_state.dart';
import '../providers/categories_provider.dart';
import '../providers/auth_provider.dart';
import '../dialogs/search_dialog.dart';

class MainMenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MenuState>(
      builder: (widgetContext, state, _) => Container(
        color: Colors.black12,
        width: double.infinity,
        child: _createMenuLayout(context, widgetContext, state),
      ),
    );
  }

  Widget _createMenuLayout(
      BuildContext rootContext, BuildContext widgetContext, MenuState state) {
    var createSubCategoryBtn = state.canCreateSubCategory
        ? TextButton(
            child: Text('Create Sub Category'),
            onPressed: () {
              _createSubCategoryDialog(widgetContext);
            },
          )
        : TextButton(child: Text('Create Sub Category'), onPressed: null);

    var saveBtn = state.hasDataToSave
        ? TextButton(
            child: Icon(Icons.save),
            onPressed: () {
              var categories = rootContext.read<CategoriesProvider>();

              var auth = rootContext.read<AuthProvider>();

              categories.save(auth.passKey);
              state.updateHasDataToSaveState();
            },
          )
        : TextButton(child: Icon(Icons.save), onPressed: null);

    var logOut = state.hasDataToSave
        ? TextButton(child: Icon(Icons.logout), onPressed: null)
        : TextButton(
            child: Icon(Icons.logout),
            onPressed: () {
              widgetContext.read<AuthProvider>().logout();
            });

    var searchBtn = state.searchWord == null || state.searchWord == ""
        ? TextButton(
            child: Icon(Icons.search),
            onPressed: () async {
              var searchWord = await SearchDialog.show(rootContext);
              rootContext.read<CategoriesProvider>().setSearchWord(searchWord);
              state.setSearchWord(searchWord);
            })
        : TextButton(
            child: Row(children: [Icon(Icons.search), Text(state.searchWord)]),
            onPressed: () async {
              var searchWord = await SearchDialog.show(rootContext);
              rootContext.read<CategoriesProvider>().setSearchWord(searchWord);
              state.setSearchWord(searchWord);
            });

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
      child: Row(
        children: <Widget>[
          searchBtn,
          Expanded(
            child: SizedBox(
              width: 10,
            ),
          ),
          TextButton(
            child: Text('Create Category'),
            onPressed: () {
              _createCategoryDialog(widgetContext);
            },
          ),
          SizedBox(
            width: 30,
          ),
          createSubCategoryBtn,
          SizedBox(
            width: 30,
          ),
          saveBtn,
          SizedBox(
            width: 30,
          ),
          logOut,
        ],
      ),
    );
  }

  Future<void> _createCategoryDialog(BuildContext context) async {
    var result = await TextInputDialog.show(context);
    if (result == null) return;
    context.read<CategoriesProvider>().addCategory(result);
    // DiskSaver.saveToDisk();
  }

  Future<void> _createSubCategoryDialog(BuildContext context) async {
    var result = await TextInputDialog.show(context);
    if (result == null) return;
    context.read<CategoriesProvider>().addSubCategory(result);
    // DiskSaver.saveToDisk();
  }
}
