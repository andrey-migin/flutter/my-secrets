import 'package:flutter/material.dart';
import 'package:my_secrets/providers/categories_provider.dart';
import 'package:my_secrets/states/menu_state.dart';
import 'package:provider/provider.dart';

class TextWidget extends StatefulWidget {
  @override
  _TextWidgetState createState() => _TextWidgetState();
}

class _TextWidgetState extends State<TextWidget> {
  TextEditingController _controller = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer<CategoriesProvider>(builder: (ctx, provider, _) {
      var text = provider.activeText;
      return text == null
          ? _createBlank()
          : _createTextWidget(context, text, provider);
    });
  }

  Widget _createBlank() {
    return Text("");
  }

  Widget _createReadOnlyWidget(
      BuildContext context, String text, CategoriesProvider textProvider) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(5),
        child: SelectableText(text),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {textProvider.setTextReadOnly(false)},
        tooltip: 'Edit',
        child: Icon(Icons.edit),
      ),
    );
  }

  Widget _createTextWidget(
      BuildContext context, String text, CategoriesProvider textProvider) {
    if (textProvider.textReadOnly)
      return _createReadOnlyWidget(context, text, textProvider);

    _controller.text = text;

    var menuState = context.read<MenuState>();

    return Card(
      elevation: 10,
      child: Padding(
        padding: EdgeInsets.all(5),
        child: Form(
          child: TextFormField(
              keyboardType: TextInputType.multiline,
              maxLines: 50,
              controller: _controller,
              onChanged: (value) {
                textProvider.updateText(value);
                menuState.updateHasDataToSaveState();
              }),
        ),
      ),
    );
  }
}
