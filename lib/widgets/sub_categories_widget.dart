import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/categories_provider.dart';

class SubCategoriesListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = context.read<CategoriesProvider>();
    return _createLayout(context, provider);
  }

  Widget _createLayout(BuildContext context, CategoriesProvider provider) {
    var items = provider.getSubCategories();
    var selectedItem = provider.selectedSubCategory;
    var widgets = <Widget>[];

    for (var i = 0; i < items.length; i++) {
      var subCategory = items[i];

      if (subCategory == selectedItem) {
        widgets.add(
          Container(
            width: double.infinity,
            child: ElevatedButton(
              child: Text(subCategory),
              onPressed: () => provider.setSelectedSubCategory(null),
            ),
          ),
        );
      } else {
        widgets.add(
          Container(
            width: double.infinity,
            child: TextButton(
              child: Text(subCategory),
              onPressed: () => provider.setSelectedSubCategory(subCategory),
            ),
          ),
        );
      }
    }

    return Container(
      width: double.infinity,
      child: Column(
        children: widgets,
      ),
    );
  }
}
