import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../states/menu_state.dart';
import '../providers/categories_provider.dart';
import '../widgets/sub_categories_widget.dart';

class CategoriesListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext rootContext) {
    return Consumer<CategoriesProvider>(
      builder: (widgetContext, provider, _) =>
          _buildLayout(widgetContext, provider),
    );
  }

  Widget _buildLayout(BuildContext widgetContext, CategoriesProvider provider) {
    var items = provider.getCategories();

    var selectedCategory = provider.selectedCategory;

    return ListView.builder(
      itemBuilder: (listItemContext, index) => _createCategoryCard(
          widgetContext,
          listItemContext,
          items[index],
          items[index] == selectedCategory),
      itemCount: items.length,
    );
  }

  Widget _createCategoryCard(BuildContext widgetContext,
      BuildContext itemContext, String category, bool selected) {
    if (selected) {
      return Column(
        children: [
          Card(
            margin: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
            color: selected ? Colors.lightGreenAccent : null,
            child: ListTile(
              title: Text(category),
              selected: selected,
              horizontalTitleGap: 0,
            ),
          ),
          SubCategoriesListWidget()
        ],
      );
    }

    return Card(
      margin: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
      color: selected ? Colors.lightGreenAccent : null,
      child: ListTile(
        title: Text(category),
        selected: selected,
        hoverColor: Color.fromARGB(50, 150, 150, 200),
        onTap: () => {_setSelectedIndex(widgetContext, category)},
      ),
    );
  }

  Future<void> _setSelectedIndex(BuildContext context, String category) async {
    var categoriesProvider = context.read<CategoriesProvider>();

    categoriesProvider.setSelectedCategory(category);

    context
        .read<MenuState>()
        .setCanCreateSubCategory(categoriesProvider.hasSelectedCategory);
  }
}
